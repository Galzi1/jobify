/* Amplify Params - DO NOT EDIT
	ENV
	REGION
Amplify Params - DO NOT EDIT */

import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { DynamoDBDocumentClient, PutCommand } from "@aws-sdk/lib-dynamodb";
import { uuid } from "uuidv4";

const client = new DynamoDBClient({});

const dynamo = DynamoDBDocumentClient.from(client);

const tableName = "companies";

/**
 * @type {import('@types/aws-lambda').APIGatewayProxyHandler}
 */
export const handler = async (event) => {
  console.log(`EVENT: ${JSON.stringify(event)}`); //TODO: remove this line in prod
  const requestItemToAdd = event.body;
  requestItemToAdd.id = uuid();
  requestItemToAdd.lastUpdated = new Date().toISOString(); //TODO: separate to general function / layer

  const resp = await dynamo.send(
    new PutCommand({
      TableName: tableName,
      Item: requestItemToAdd,
    })
  );

  return {
    statusCode: resp.$metadata?.httpStatusCode || 200,
    //  Uncomment below to enable CORS requests
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers": "*",
      "Content-Type": "application/json",
    },
    body: requestItemToAdd,
  };
};
