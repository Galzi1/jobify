/* Amplify Params - DO NOT EDIT
	ENV
	REGION
Amplify Params - DO NOT EDIT */

import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { DynamoDBDocumentClient, DeleteCommand } from "@aws-sdk/lib-dynamodb";

const client = new DynamoDBClient({});

const dynamo = DynamoDBDocumentClient.from(client);

const tableName = "companies";

/**
 * @type {import('@types/aws-lambda').APIGatewayProxyHandler}
 */
export const handler = async (event) => {
  console.log(`EVENT: ${JSON.stringify(event)}`); //TODO: remove this line in prod
  const requestItemIdToRemove = event.body;

  const resp = await dynamo.send(
    new DeleteCommand({
      TableName: tableName,
      Key: requestItemIdToRemove,
    })
  );

  return {
    statusCode: resp.$metadata?.httpStatusCode || 200,
    //  Uncomment below to enable CORS requests
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers": "*",
      "Content-Type": "text",
    },
    body: requestItemIdToRemove,
  };
};
