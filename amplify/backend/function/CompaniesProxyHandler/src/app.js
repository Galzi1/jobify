const express = require("express");
const bodyParser = require("body-parser");
const awsServerlessExpressMiddleware = require("aws-serverless-express/middleware");
const { DynamoDBClient } = require("@aws-sdk/client-dynamodb");
const {
  DynamoDBDocumentClient,
  ScanCommand,
  GetCommand,
  PutCommand,
  DeleteCommand,
} = require("@aws-sdk/lib-dynamodb");
const { uuid } = require("uuidv4");

const app = express();
app.use(bodyParser.json());
app.use(awsServerlessExpressMiddleware.eventContext());

// Enable CORS for all methods
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  next();
});

const dynamoClient = new DynamoDBClient({});
const dynamoDocumentClient = DynamoDBDocumentClient.from(dynamoClient);
const TABLE_NAME = "companies";

app.get("/companies", async function (req, res) {
  const resp = await dynamoDocumentClient.send(
    new ScanCommand({ TableName: TABLE_NAME })
  );
  const companies = resp.Items;
  res.json(companies);
});

app.get("/companies/:companyId", async function (req, res) {
  const resp = await dynamoDocumentClient.send(
    new GetCommand({
      TableName: TABLE_NAME,
      Key: { id: req.params.companyId },
    })
  );
  const company = resp.Item;
  res.json(company);
});

app.post("/companies", async function (req, res) {
  const requestItemToAdd =
    typeof req.body === "string" ? JSON.parse(req.body) : req.body;
  requestItemToAdd.id = uuid();
  requestItemToAdd.lastUpdated = new Date().toISOString(); //TODO: separate to general function / layer

  const dynamoResp = await dynamoDocumentClient.send(
    new PutCommand({
      TableName: TABLE_NAME,
      Item: requestItemToAdd,
    })
  );

  res
    .status(dynamoResp.$metadata?.httpStatusCode || 200)
    .json(requestItemToAdd);
});

app.put("/companies", async function (req, res) {
  const requestItemToUpdate =
    typeof req.body === "string" ? JSON.parse(req.body) : req.body;
  requestItemToUpdate.lastUpdated = new Date().toISOString(); //TODO: separate to general function / layer

  const dynamoResp = await dynamoDocumentClient.send(
    new PutCommand({
      TableName: TABLE_NAME,
      Item: requestItemToUpdate,
    })
  );

  res
    .status(dynamoResp.$metadata?.httpStatusCode || 200)
    .json(requestItemToUpdate);
});

app.delete("/companies/:companyId", async function (req, res) {
  const requestItemIdToRemove = req.params.companyId;
  const dynamoResp = await dynamoDocumentClient.send(
    new DeleteCommand({
      TableName: TABLE_NAME,
      Key: { id: requestItemIdToRemove },
    })
  );

  res
    .status(dynamoResp.$metadata?.httpStatusCode || 200)
    .json(requestItemIdToRemove);
});

app.listen(3000, function () {
  console.log("App started");
});

module.exports = app;
