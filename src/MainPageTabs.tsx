import { Box, Tab, Tabs } from "@mui/material";
import React, { PropsWithChildren, useState } from "react";
import CompaniesPage from "./companies/CompaniesPage";

type TabPanelProps = PropsWithChildren<{
  activeTabIndex: number;
  index: number;
}>;

function TabPanel(props: TabPanelProps) {
  const { children, activeTabIndex, index } = props;

  return (
    <div
      role="tabpanel"
      hidden={activeTabIndex !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
    >
      {activeTabIndex === index && <Box sx={{ p: 2 }}>{children}</Box>}
    </div>
  );
}

export default function MainPageTabs() {
  const [activeTabIndex, setActiveTabIndex] = useState(0);

  const handleTabChange = (
    _: React.SyntheticEvent<Element, Event>,
    newValue: number
  ) => {
    setActiveTabIndex(newValue);
  };

  return (
    <>
      <Tabs value={activeTabIndex} onChange={handleTabChange}>
        <Tab label="Companies" />
        <Tab label="Listings" />
      </Tabs>
      <TabPanel activeTabIndex={activeTabIndex} index={0}>
        <CompaniesPage />
      </TabPanel>
      <TabPanel activeTabIndex={activeTabIndex} index={1}>
        Listings
      </TabPanel>
    </>
  );
}
