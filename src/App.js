import "./App.css";
import MainPageTabs from "./MainPageTabs";

function App() {
  return (
    <div className="App">
      <MainPageTabs />
    </div>
  );
}

export default App;
