import React from "react";
import { Box, Button, DialogActions, TextField } from "@mui/material";
import { FormikProps } from "formik";
import { Company } from "../types";

export default function NewCompanyForm(props: FormikProps<Company>) {
  const {
    values,
    touched,
    errors,
    dirty,
    isSubmitting,
    isValid,
    handleChange,
    handleBlur,
    handleSubmit,
    handleReset,
  } = props;

  return (
    <form onSubmit={handleSubmit}>
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <TextField
          required
          error={!!errors.name && !!touched.name}
          label="Name"
          name="name"
          onChange={handleChange}
          onBlur={handleBlur}
          margin="normal"
          defaultValue={values.name || ""}
          variant="standard"
          sx={{ marginX: 1 }}
        />
        <TextField
          error={!!errors.address && !!touched.address}
          label="Address"
          name="address"
          onChange={handleChange}
          onBlur={handleBlur}
          margin="normal"
          defaultValue={values.address || ""}
          variant="standard"
          sx={{ marginX: 1 }}
        />
      </Box>
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <TextField
          error={!!errors.linkedinProfile && !!touched.linkedinProfile}
          label="LinkedIn Profile"
          name="linkedinProfile"
          onChange={handleChange}
          onBlur={handleBlur}
          margin="normal"
          defaultValue={values.linkedinProfile || ""}
          variant="standard"
          sx={{ marginX: 1 }}
        />
        <TextField
          required
          error={!!errors.referenceType && !!touched.referenceType}
          label="Reference Type"
          name="referenceType"
          onChange={handleChange}
          onBlur={handleBlur}
          margin="normal"
          defaultValue={values.referenceType || ""}
          variant="standard"
          sx={{ marginX: 1 }}
        />
      </Box>
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <TextField
          required
          error={!!errors.referrer && !!touched.referrer}
          label="Referrer"
          name="referrer"
          onChange={handleChange}
          onBlur={handleBlur}
          margin="normal"
          defaultValue={values.referrer || ""}
          variant="standard"
          sx={{ marginX: 1 }}
        />
        <TextField
          error={!!errors.technologyScore && !!touched.technologyScore}
          label="Technology Score"
          name="technologyScore"
          onChange={handleChange}
          onBlur={handleBlur}
          margin="normal"
          defaultValue={values.technologyScore || ""}
          variant="standard"
          sx={{ marginX: 1 }}
        />
      </Box>
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <TextField
          error={!!errors.productScore && !!touched.productScore}
          label="Product Score"
          name="productScore"
          onChange={handleChange}
          onBlur={handleBlur}
          margin="normal"
          defaultValue={values.productScore || ""}
          variant="standard"
          sx={{ marginX: 1 }}
        />
        <TextField
          error={!!errors.economicScore && !!touched.economicScore}
          label="Economic Score"
          name="economicScore"
          onChange={handleChange}
          onBlur={handleBlur}
          margin="normal"
          defaultValue={values.economicScore || ""}
          variant="standard"
          sx={{ marginX: 1 }}
        />
      </Box>
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <TextField
          error={!!errors.locationScore && !!touched.locationScore}
          label="Location Score"
          name="locationScore"
          onChange={handleChange}
          onBlur={handleBlur}
          margin="normal"
          defaultValue={values.locationScore || ""}
          variant="standard"
          sx={{ marginX: 1 }}
        />
      </Box>
      <DialogActions>
        <Button
          type="button"
          className="outline"
          onClick={handleReset}
          disabled={!dirty || isSubmitting}
        >
          Reset
        </Button>
        {/* <Button onClick={() => setNewCompanyDialogOpen(false)}>Cancel</Button> */}
        <Button type="submit" disabled={!dirty || isSubmitting || !isValid}>
          Submit
        </Button>
      </DialogActions>
    </form>
  );
}
