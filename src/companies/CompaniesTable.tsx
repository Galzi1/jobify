import { Paper } from "@mui/material";
import {
  DataGrid,
  GridRowParams,
  GridActionsCellItem,
  GridColDef,
} from "@mui/x-data-grid";
import React from "react";
import { Company } from "../types";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";

type CompaniesTableProps = {
  companies: Array<Company>;
  deleteCompanyCallback: (id: string) => void;
  editCompanyCallback: (values: Company) => void;
};

export default function companiesTable(props: CompaniesTableProps) {
  const { companies, deleteCompanyCallback, editCompanyCallback } = props;

  const columns: Array<GridColDef<Company>> = [
    { field: "name", headerName: "Name", editable: true, flex: 1 },
    { field: "address", headerName: "Address", editable: true, flex: 1 },
    {
      field: "linkedinProfile",
      headerName: "LinkedIn",
      editable: true,
      flex: 1,
    },
    {
      field: "referenceType",
      headerName: "Reference Type",
      editable: true,
      flex: 1,
    },
    { field: "referrer", headerName: "Referrer", editable: true, flex: 1 },
    {
      field: "technologyScore",
      headerName: "Tech. Score",
      editable: true,
      flex: 1,
    },
    {
      field: "productScore",
      headerName: "Prod. Score",
      editable: true,
      flex: 1,
    },
    {
      field: "economicScore",
      headerName: "Eco. Score",
      editable: true,
      flex: 1,
    },
    {
      field: "locationScore",
      headerName: "Loc. Score",
      editable: true,
      flex: 1,
    },
    { field: "overallScore", headerName: "Overall Score", flex: 1 },
    {
      field: "lastUpdated",
      headerName: "Last Updated",
      flex: 1,
      valueFormatter: (params: any) => new Date(params?.value).toLocaleString(),
    },
    {
      field: "actions",
      type: "actions",
      getActions: (params: GridRowParams<Company>) => [
        <GridActionsCellItem
          icon={<DeleteIcon />}
          label="Delete"
          onClick={() => deleteCompanyCallback(params.id.toString())}
        />,
        <GridActionsCellItem
          icon={<EditIcon />}
          label="Edit"
          onClick={() => editCompanyCallback(params.row)}
        />,
      ],
    },
  ];

  return (
    <Paper sx={{ width: "100%", mb: 2 }}>
      <DataGrid rows={companies} columns={columns} autoHeight />
    </Paper>
  );
}
