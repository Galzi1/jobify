import CompaniesTable from "./CompaniesTable";
import { Company } from "../types";
import React, { useState, useMemo } from "react";
import { Box, Button } from "@mui/material";
import NewCompanyForm from "./NewCompanyForm";
import * as Yup from "yup";
import useCompaniesAPI from "./useCompaniesAPI";
import NewCompanyFormDialog from "./NewCompanyFormDialog";

const NEW_COMPANY_FORM_TITLE = "New Company";
const EDIT_COMPANY_FORM_TITLE = "Edit Company";

const COMPANY_INITIAL_VALUES: Company = Object.freeze({
  id: "",
  name: "",
  referenceType: "",
  referrer: "",
});

const companyValidationSchema = Yup.object({
  name: Yup.string().required(),
  address: Yup.string().nullable(),
  linkedinProfile: Yup.string().url().nullable(),
  referenceType: Yup.string().required(),
  referrer: Yup.string().required(),
  overallScore: Yup.number().nullable(),
  technologyScore: Yup.number().integer().nullable(),
  productScore: Yup.number().integer().nullable(),
  economicScore: Yup.number().integer().nullable(),
  locationScore: Yup.number().integer().nullable(),
  lastUpdated: Yup.date().nullable(),
});

export default function CompaniesPage() {
  const [newCompanyDialogOpen, setNewCompanyDialogOpen] = useState(false);
  const [editCompanyDialogOpen, setEditCompanyDialogOpen] = useState(false);

  const [companyInitialValues, setCompanyInitialValues] = useState(
    COMPANY_INITIAL_VALUES
  );

  const {
    companiesData,
    triggerAddCompany,
    triggerDeleteCompany,
    triggerEditCompany,
  } = useCompaniesAPI();

  const openNewCompanyDialog = () => {
    setNewCompanyDialogOpen(true);
  };

  function openEditCompanyDialog(companyToEdit: Company) {
    setCompanyInitialValues(companyToEdit);
    setEditCompanyDialogOpen(true);
  }

  function onCloseNewCompanyDialog() {
    setNewCompanyDialogOpen(false);
  }

  function onCloseEditCompanyDialog() {
    setEditCompanyDialogOpen(false);
    setCompanyInitialValues(COMPANY_INITIAL_VALUES);
  }

  const newCompanyFormComponent = useMemo(
    () => (
      <NewCompanyFormDialog
        initialValues={COMPANY_INITIAL_VALUES}
        onSubmit={triggerAddCompany}
        open={newCompanyDialogOpen}
        closeForm={onCloseNewCompanyDialog}
        title={NEW_COMPANY_FORM_TITLE}
        formRenderer={NewCompanyForm}
        validationSchema={companyValidationSchema}
      />
    ),
    [triggerAddCompany, newCompanyDialogOpen]
  );

  const editCompanyFormComponent = useMemo(
    () => (
      <NewCompanyFormDialog
        initialValues={companyInitialValues}
        onSubmit={triggerEditCompany}
        open={editCompanyDialogOpen}
        closeForm={onCloseEditCompanyDialog}
        title={EDIT_COMPANY_FORM_TITLE}
        formRenderer={NewCompanyForm}
        validationSchema={companyValidationSchema}
        customInitialValues={true}
      />
    ),
    [triggerEditCompany, editCompanyDialogOpen, companyInitialValues]
  );

  return (
    <div>
      <Box sx={{ display: "flex", flexDirection: "row" }}>
        <Button variant="contained" onClick={openNewCompanyDialog}>
          New Company
        </Button>
      </Box>
      <br />
      {newCompanyFormComponent}
      {editCompanyFormComponent}
      <CompaniesTable
        companies={companiesData || []}
        deleteCompanyCallback={triggerDeleteCompany}
        editCompanyCallback={openEditCompanyDialog}
      />
    </div>
  );
}
