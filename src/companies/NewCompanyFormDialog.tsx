import * as React from "react";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import * as Yup from "yup";
import { Formik, FormikProps } from "formik";
import { Company } from "../types";

type FormDialogProps = {
  initialValues: Company;
  validationSchema?: Yup.Schema;
  onSubmit: (values: Company) => void;
  open: boolean;
  closeForm: () => void;
  title?: string;
  formRenderer: (props: FormikProps<Company>) => JSX.Element;
  customInitialValues?: boolean;
};

export default function NewCompanyFormDialog(props: FormDialogProps) {
  const {
    initialValues,
    validationSchema,
    onSubmit,
    open,
    closeForm,
    title,
    formRenderer,
    customInitialValues,
  } = props;

  const handleClose = () => {
    closeForm();
  };

  const onSubmitForm = (values: Company) => {
    onSubmit(values);
    handleClose();
  };

  return (
    <Dialog open={open} onClose={handleClose}>
      {title && <DialogTitle>{title}</DialogTitle>}
      <DialogContent>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={onSubmitForm}
          enableReinitialize={customInitialValues}
        >
          {formRenderer}
        </Formik>
      </DialogContent>
    </Dialog>
  );
}
