import { API } from "aws-amplify";
import useSWRMutation from "swr/mutation";
import useSWR from "swr";
import { Company } from "../types";
import { logError } from "../utils/logger";
import { useLazySWR } from "../utils/useLazySWR";

const COMPANIES_API_ID = "apie3640045";
const COMPANIES_API_PATH = "/companies";

async function getCompaniesFetcher(url: string) {
  return API.get(COMPANIES_API_ID, url, {});
}

async function getCompanyFetcher(url: string, { arg }: { arg: string }) {
  const companyToDeletePath = url + "/" + arg;
  return API.get(COMPANIES_API_ID, companyToDeletePath, {});
}

async function addCompanyFetcher(url: string, { arg }: { arg: Company }) {
  return API.post(COMPANIES_API_ID, COMPANIES_API_PATH, { body: arg });
}

async function deleteCompanyFetcher(url: string, { arg }: { arg: string }) {
  if (!window.confirm("Are you sure you want to delete this company?")) {
    return;
  }
  const companyToDeletePath = url + "/" + arg;
  return API.del(COMPANIES_API_ID, companyToDeletePath, {});
}

async function editCompanyFetcher(url: string, { arg }: { arg: Company }) {
  return API.put(COMPANIES_API_ID, COMPANIES_API_PATH, { body: arg });
}

export default function useCompaniesAPI() {
  const {
    data: companiesData,
    error: companiesError,
    isLoading: companiesIsLoading,
  } = useSWR<Company[]>(COMPANIES_API_PATH, getCompaniesFetcher, {
    onError: (err) => {
      logError(err);
    },
  });

  const getCompany = useLazySWR({
    baseURL: COMPANIES_API_PATH,
    fetcher: getCompanyFetcher,
  });

  const { trigger: triggerAddCompany } = useSWRMutation(
    COMPANIES_API_PATH,
    addCompanyFetcher,
    {
      onError: (err) => {
        logError(err);
      },
    }
  );

  const { trigger: triggerDeleteCompany } = useSWRMutation(
    COMPANIES_API_PATH,
    deleteCompanyFetcher,
    {
      onError: (err) => {
        logError(err);
      },
    }
  );

  const { trigger: triggerEditCompany } = useSWRMutation(
    COMPANIES_API_PATH,
    editCompanyFetcher,
    {
      onError: (err) => {
        logError(err);
      },
    }
  );

  return {
    companiesData,
    companiesError,
    companiesIsLoading,
    getCompany,
    triggerAddCompany,
    triggerDeleteCompany,
    triggerEditCompany,
  };
}
