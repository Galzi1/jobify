import useSWR from "swr";
import { useState } from "react";
import lodash from "lodash";

export const useLazySWR = ({
  baseURL,
  fetcher,
}: {
  baseURL: string;
  fetcher: (url: string, { arg }: { arg: string }) => Promise<any>;
}) => {
  // We're going to use SWR conditional fetching,
  // so we have to set the trigger for the request.
  const [variables, setVariables] = useState<any | undefined>();

  // Using "variables", we say SWR when to make the request.
  const { data } = useSWR(variables && [baseURL, variables], fetcher, {
    suspense: true,
  });

  // This promise function will allow us to access the lazy hook.
  const executeQuery = (parameters: any) =>
    new Promise((resolve, reject) => {
      // Reject the promise if has the same parameters.
      if (lodash.isEqual(parameters, variables))
        reject(new Error("Requested twice with the same parameters"));

      // Set the variables to make the request.
      setVariables({ ...parameters });

      // Resolve the promise.
      resolve(undefined);
    });

  return [executeQuery, data];
};
