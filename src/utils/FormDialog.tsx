import * as React from "react";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import * as Yup from "yup";
import { Formik, FormikProps, FormikValues } from "formik";

type FormDialogProps<T> = {
  initialValues: T;
  validationSchema?: Yup.Schema;
  onSubmit: (values: T) => void;
  open: boolean;
  closeForm: () => void;
  title?: string;
  formRenderer: (props: FormikProps<T>) => JSX.Element;
};

export default function FormDialog<T extends FormikValues>(
  props: FormDialogProps<T>
) {
  const {
    initialValues,
    validationSchema,
    onSubmit,
    open,
    closeForm,
    title,
    formRenderer,
  } = props;

  const handleClose = () => {
    closeForm();
  };

  const onSubmitForm = (values: T) => {
    onSubmit(values);
    handleClose();
  };

  return (
    <Dialog open={open} onClose={handleClose}>
      {title && <DialogTitle>{title}</DialogTitle>}
      <DialogContent>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={onSubmitForm}
        >
          {formRenderer}
        </Formik>
      </DialogContent>
    </Dialog>
  );
}
