import { FormikValues } from "formik";

export interface Company extends FormikValues {
  id: string;
  name: string;
  address?: string;
  linkedinProfile?: string;
  referenceType: string;
  referrer: string;
  overallScore?: number;
  technologyScore?: number;
  productScore?: number;
  economicScore?: number;
  locationScore?: number;
  lastUpdated?: string;
}
